#!/bin/bash
cd /opt/plasonup
source venv/bin/activate
cd /opt/plasonup/plasonup
gunicorn plasonup.wsgi -t 600 -b 127.0.0.1:8018 -w 6 --user=servidor --group=servidor --log-file=/opt/plasonup/gunicorn.log 2>>/opt/plasonup/gunicorn.log
