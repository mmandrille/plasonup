#Imports de python

#Realizamos imports de Django
from django.db import models
from django.utils import timezone
from .choices import TIPO_TRABAJO, TIPO_BENEFICIO
from django.urls import reverse

# Create your models here.
class Departamento(models.Model):
    nombre = models.CharField('Nombre', max_length=8, blank=True, null=True)
    def __str__(self):
        return self.nombre

class Localidad(models.Model):
    departamento = models.ForeignKey(Departamento, on_delete=models.SET_NULL, blank=True, null=True)
    nombre = models.CharField('Nombre', max_length=8, blank=True, null=True)
    def __str__(self):
        return self.nombre

class Sector(models.Model):
    nombre = models.CharField('Nombre', max_length=8, blank=True, null=True)
    def __str__(self):
        return self.nombre

class Ambito(models.Model):
    nombre = models.CharField('Nombre', max_length=8, blank=True, null=True)
    def __str__(self):
        return self.nombre

class Escuela(models.Model):
    cueanexo = models.IntegerField('CUE',)
    nombre = models.CharField('Nombre', max_length=100)
    localidad = models.ForeignKey(Localidad, on_delete=models.SET_NULL, blank=True, null=True)
    sector = models.ForeignKey(Sector, on_delete=models.SET_NULL, blank=True, null=True)
    ambito = models.ForeignKey(Ambito, on_delete=models.SET_NULL, blank=True, null=True)
    def __str__(self):
        return self.nombre + ' de ' + self.localidad.nombre

class Beneficiario(models.Model):
    tarjeta = models.CharField('Tarjeta', max_length=100, unique=True)
    dni = models.CharField('DNI', max_length=8,)
    cuil = models.CharField('CUIL', max_length=13, blank=True, null=True)
    apellidos = models.CharField('Apellidos', max_length=100)
    nombres = models.CharField('Nombres', max_length=100)
    tel_celular = models.CharField('Telefono Celular', max_length=100, blank=True, null=True)
    tel_fijo = models.CharField('Telefono Fijo', max_length=100, blank=True, null=True)
    email = models.EmailField('Correo Personal', blank=True, null=True)
    fecha_nacimiento = models.DateField('Fecha de Nacimiento', blank=True, null=True)
    localidad = models.ForeignKey(Localidad, on_delete=models.SET_NULL, blank=True, null=True)
    direccion_calle = models.CharField('Calle', max_length=50, blank=True, null=True)
    direccion_numero = models.CharField('Numero', max_length=50, blank=True, null=True)
    direccion_barrio = models.CharField('Barrio', max_length=50, blank=True, null=True)
    direccion_manzana = models.CharField('Manzana', max_length=50, blank=True, null=True)
    direccion_lote = models.CharField('Lote', max_length=50, blank=True, null=True)
    def __str__(self):
        return self.apellidos + ' ' + self.nombres
    def get_absolute_url(self):
        return reverse('completar_beneficiario', kwargs={'benef_id': self.id, 'benef_tarjeta': self.tarjeta})
    

class Parentezco(models.Model):
    nombre = models.CharField('tipo', max_length=100)
    def __str__(self):
        return self.nombre

class Criticidad(models.Model):
    nombre = models.CharField('tipo', max_length=100)
    def __str__(self):
        return self.nombre

class Familiar(models.Model):
    beneficiario = models.ForeignKey(Beneficiario, on_delete=models.CASCADE, related_name='familiares')
    dni = models.CharField('DNI', max_length=8)
    apellidos = models.CharField('Apellidos', max_length=100)
    nombres = models.CharField('Nombres', max_length=100)
    fecha_nacimiento = models.DateField('Fecha de Nacimiento', blank=True, null=True)
    escuela = models.ForeignKey(Escuela, on_delete=models.SET_NULL, blank=True, null=True)
    parentezco = models.ForeignKey(Parentezco, on_delete=models.CASCADE)
    criticidad = models.ForeignKey(Criticidad, on_delete=models.SET_NULL, blank=True, null=True)
    trabajo = models.CharField('Tipo de Trabajo', choices=TIPO_TRABAJO, max_length=2, default='NT', null=True, blank=True)    
    beneficio = models.CharField('Recibe beneficio', choices=TIPO_BENEFICIO, max_length=3 , default='NT', null=True, blank=True)
    def __str__(self):
        return self.apellidos + ' ' + self.nombres


class Conyuge(models.Model):
    beneficiario = models.ForeignKey(Beneficiario, on_delete=models.CASCADE, related_name='conyuges')
    nombres = models.CharField('Nombres', max_length=100)
    apellidos = models.CharField('Apellidos', max_length=100)
    dni = models.CharField('DNI', max_length=8,)
    cuil = models.CharField('CUIL', max_length=13, blank=True, null=True)
    trabajo = models.CharField('Tipo de Trabajo', choices=TIPO_TRABAJO, max_length=2, default='NT', null=True, blank=True)    
    beneficio = models.CharField('Recibe beneficio', choices=TIPO_BENEFICIO, max_length=3 , default='NT', null=True, blank=True)
    def __str__(self):
        return self.apellidos + ' ' + self.nombres

