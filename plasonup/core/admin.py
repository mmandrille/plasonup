from django.contrib import admin

#Modulos para no permitir escalacion de privilegios
from django.contrib.auth.admin import UserAdmin, User

#Import del proyecto
from core.models import Departamento, Localidad, Ambito, Escuela
from core.models import Beneficiario, Parentezco, Familiar, Criticidad, Conyuge

#Modificacion del panel de administrador para no permitir escalacion de privilegios
class RestrictedUserAdmin(UserAdmin):
    model = User
    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(RestrictedUserAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        user = kwargs['request'].user
        if not user.is_superuser:
            if db_field.name == 'groups':
                field.queryset = field.queryset.filter(id__in=[i.id for i in user.groups.all()])
            if db_field.name == 'user_permissions':
                field.queryset = field.queryset.filter(id__in=[i.id for i in user.user_permissions.all()])
            if db_field.name == 'is_superuser':
                field.widget.attrs['disabled'] = True
        return field

#Define modifications
class LocalidadInline(admin.TabularInline):
    model = Localidad
    ordering = ['nombre']

class DepartamentoAdmin(admin.ModelAdmin):
    ordering = ['nombre']
    inlines = [LocalidadInline, ]

class LocalidadAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_filter = ['departamento', ]

class EscuelaAdmin(admin.ModelAdmin):
    model = Escuela
    ordering = ['cueanexo',]
    search_fields = ['nombre']
    list_filter = ['localidad__departamento', 'localidad', 'sector', 'ambito',]

class FamiliarInline(admin.TabularInline):
    model = Familiar
    raw_id_fields = ("escuela",)
    ordering = ['apellidos', 'nombres']
    autocomplete_fields = ['escuela']

class ConyugeInline(admin.TabularInline):
    model = Conyuge    
    ordering = ['apellidos', 'nombres']
    

class BeneficiarioAdmin(admin.ModelAdmin):
    ordering = ['apellidos', 'nombres', ]
    search_fields = ['nombres', 'apellidos', 'dni', 'tarjeta',]
    list_filter = ['localidad__departamento', 'localidad']
    inlines = [FamiliarInline, ConyugeInline, ]
    autocomplete_fields = ['localidad']
    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["dni", "tarjeta"]
        else:
            return []

#Registramos modificaciones para no permitir escalacion de privilegios
admin.site.unregister(User)
admin.site.register(User, RestrictedUserAdmin)
# Register your models here.
admin.site.register(Departamento, DepartamentoAdmin)
admin.site.register(Localidad, LocalidadAdmin)
admin.site.register(Escuela, EscuelaAdmin)
admin.site.register(Beneficiario, BeneficiarioAdmin)
admin.site.register(Parentezco, )
admin.site.register(Criticidad, )