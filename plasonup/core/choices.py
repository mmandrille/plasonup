TIPO_TRABAJO = (
    ('NT', 'No trabaja'), 
    ('TB', 'Trabajo en Blanco'),   
    ('TI', 'Trabajo Informal'),
    ('O', 'Otro...'),
)

TIPO_BENEFICIO = (
    ('NT', 'No tiene'), 
    ('TAF', 'Titular de asignacion familiar'),   
    ('TBP', 'Titular de beca progresar'),
    ('PD', 'Cobra prestacion por desempleo'),
    ('TES', 'Programa de Empleo de la Secretaria de Trabajo, Empleo y Seguridad Social'),
    ('J', 'Jubilado'),
    ('O', 'Otro'),
)