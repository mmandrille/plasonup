#Modulos Python
import csv
#Modulos Django
from django.shortcuts import render
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib import messages 
#Imports del proyecto
from .decoradores import superuser_required
from .forms import UploadCsv
from .models import Departamento, Localidad, Escuela
from .models import Sector, Ambito
from .models import Beneficiario, Familiar, Conyuge
from .forms import TarjetaForm, BeneficiarioForm, FamiliarFormset, ConyugeFormset
# Create your views here.
def home(request):
    return render(request, 'home.html', {})

@superuser_required
def upload_escuelas(request):
    titulo = "Gestion Masiva de Escuelas"
    if request.method == "GET":
        form = UploadCsv()
        return render(request, 'upload_csv.html', {'titulo': titulo, 'form': form,})
    elif request.method == "POST":
        form = UploadCsv(request.POST, request.FILES)
        if form.is_valid():
            count = 0
            csv_file = form.cleaned_data['csvfile']
            file_data = csv_file.read().decode("utf-8")
            lines = file_data.split("\n")
            #Procesamos el archivo
            #0-CUEANEXO	1-NOMBRE	2-CALLE	3-NRO	4-BARRIO	5-C.P.	6-LOCALIDAD	7-DEPARTAMENTO	8-TELEFONO	9-REGION	10-SECTOR	11-AMBITO
            escuelas = []
            for line in lines:
                line=line.split(',')
                if line[0]:
                    escuela = Escuela()
                    escuela.cueanexo = line[0]
                    escuela.nombre = line[1]
                    escuela.localidad = Localidad.objects.get_or_create(
                        departamento=Departamento.objects.get_or_create(nombre=line[7])[0],
                        nombre=line[6],
                    )[0]
                    escuela.sector = Sector.objects.get_or_create(nombre=line[10])[0]
                    escuela.ambito = Ambito.objects.get_or_create(nombre=line[11])[0]
                    escuelas.append(escuela)
            count = len(escuelas)
            Escuela.objects.bulk_create(escuelas)
            return render(request, "upload_csv.html", {'titulo': titulo, 'count': count, })
        else:
            message = form.errors
            return render(request, "upload_csv.html", {'titulo': titulo, 'message': message, })

@superuser_required
def upload_tarjetas(request):
    titulo = "Gestion Masiva de Tarjetas de Beneficiarios"
    if request.method == "GET":
        form = UploadCsv()
        return render(request, 'upload_csv.html', {'titulo': titulo, 'form': form, })
    elif request.method == "POST":
        form = UploadCsv(request.POST, request.FILES)
        if form.is_valid():
            count = 0
            csv_file = form.cleaned_data['csvfile']
            file_data = csv_file.read().decode("utf-8")
            lines = file_data.split("\n")
            #Procesamos el archivo
            #Beneficiario.objects.all().delete()
            beneficiarios = []
            #0-Apellido	1-Nombre	2-DNI	3-tarjeta	4-localidad
            tarjeta_cargada = []
            for line in lines:
                line=line.split(',')
                if line[0]:
                    beneficiario = Beneficiario()                        
                    beneficiario.apellidos = line[0]                    
                    beneficiario.tarjeta = line[1]
                    beneficiario.dni = line[2]
                    beneficiario.nombres = line[3]
                    if beneficiario.tarjeta not in tarjeta_cargada:
                        beneficiarios.append(beneficiario)
                        tarjeta_cargada.append(beneficiario.tarjeta)
            count = len(beneficiarios)
            Beneficiario.objects.bulk_create(beneficiarios)
            return render(request, "upload_csv.html", {'titulo': titulo, 'count': count, })
        else:
            message = form.errors
            return render(request, "upload_csv.html", {'titulo': titulo, 'message': message, })


@superuser_required
def download_beneficiarios(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="beneficiarios.csv"'
    writer = csv.writer(response)
    writer.writerow(['LISTADO DE BENEFICIARIOS'])
    writer.writerow(['DNI', 'Tarjeta', 'Cuil', 'Apellido', 'Nombre', 'Tel Celular' ,'Tel Fijo', 'Email', 'Fecha Nacimiento', 'Tiene Conyuge', 'Localidad', 'Departamento', 'Direccion Calle', 'Direccion Numero', 'Barrio', 'Manzana', 'Lote'])
    beneficiarios = Beneficiario.objects.all()
    beneficiarios = beneficiarios.select_related('localidad','localidad__departamento')
    for benef in beneficiarios:
        try:
            conyuge = Conyuge.objects.get(beneficiario_id = benef.id)
            tmp_conyu = 'SI' 
        except Conyuge.DoesNotExist:
            tmp_conyu = 'NO'      

        if benef.localidad: 
            tmp_localidad = benef.localidad.nombre
            tmp_departamento = benef.localidad.departamento.nombre
        else: 
            tmp_localidad = ''
            tmp_departamento = ''
        
        writer.writerow([
            benef.dni,
            benef.tarjeta,
            benef.cuil,
            benef.apellidos,
            benef.nombres,
            benef.tel_celular,
            benef.tel_fijo,
            benef.email,
            benef.fecha_nacimiento,
            tmp_conyu,
            tmp_localidad,
            tmp_departamento,
            benef.direccion_calle,
            benef.direccion_numero,
            benef.direccion_barrio,
            benef.direccion_manzana,
            benef.direccion_lote,])
    return response   

@superuser_required
def download_familiares(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="familiares.csv"'
    writer = csv.writer(response)
    writer.writerow(['LISTADO DE FAMILIARES DE BENEFICIARIOS'])
    writer.writerow(['Beneficiario_DNI', 'Parentezco', 'DNI', 'Apellidos', 'Nombres', 'Fecha Nacimiento', 'Escuela CUE', 'Criticidad'])
    familiares = Familiar.objects.all()
    familiares = familiares.select_related('parentezco', 'criticidad', 'escuela','beneficiario')
    for familiar in familiares:
        if familiar.escuela:
            tmp_escuela = familiar.escuela.cueanexo
        else:
            tmp_escuela = ''
        if familiar.criticidad:
            tmp_criticidad = familiar.criticidad.nombre
        else:
            tmp_criticidad = ''

        writer.writerow([
            familiar.beneficiario.dni,
            familiar.parentezco.nombre,
            familiar.dni,
            familiar.apellidos,
            familiar.nombres,
            familiar.fecha_nacimiento,
            tmp_escuela,
            tmp_criticidad])
    return response

@superuser_required
def download_conyuges(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="conyuges.csv"'
    writer = csv.writer(response)
    writer.writerow(['LISTADO DE CONYUGES DE BENEFICIARIOS'])
    writer.writerow(['Beneficiario_DNI', 'apellido_beneficiario', 'nombre_beneficiario', 'nombres', 'apellidos', 'dni', 'cuil', 'trabajo', 'beneficio'])
    conyuges = Conyuge.objects.all()
    conyuges = conyuges.select_related('beneficiario')
    for conyuge in conyuges:
        if conyuge.trabajo == 'NT':
            con_trabajo =  'No trabaja'
        if conyuge.trabajo == 'TB':
            con_trabajo = 'Trabajo en Blanco'
        if conyuge.trabajo == 'TI':
            con_trabajo = 'Trabajo Informal'
        if conyuge.trabajo == 'O':
            con_trabajo = 'Otro'

        if conyuge.beneficio == 'NT':
            con_ben =  'No tiene'
        if conyuge.beneficio == 'TAF':
            con_ben = 'Titular de asignacion familiar'
        if conyuge.beneficio == 'TBP':
            con_ben = 'Titular de beca progresar'
        if conyuge.beneficio == 'PD':
            con_ben = 'Cobra prestacion por desempleo'
        if conyuge.beneficio == 'TES':
            con_ben = 'Programa de Empleo de la Secretaria de Trabajo, Empleo y Seguridad Social'
        if conyuge.beneficio == 'J':
            con_ben = 'Jubilado'
        if conyuge.beneficio == 'O':
            con_ben = 'Otro'

        writer.writerow([
            conyuge.beneficiario.dni,
            conyuge.beneficiario.apellidos,
            conyuge.beneficiario.nombres,
            conyuge.nombres,
            conyuge.apellidos,
            conyuge.dni,            
            conyuge.cuil,
            con_trabajo,
            con_ben])
    return response

def get_tarjeta(request):
    if request.method == 'GET':
        form = TarjetaForm()
        return render(request, 'extras/generic_form.html', {'titulo': "INGRESE DATOS", 'form': form, 'boton': "ACCEDER", })
    elif request.method == 'POST':
        form = TarjetaForm(request.POST)
        if form.is_valid():
            benef_tarj = form.cleaned_data.get('tarjeta')
            benef_dni = form.cleaned_data.get('dni')
            try:
                beneficiario = Beneficiario.objects.get(tarjeta=benef_tarj, dni=benef_dni)
                return redirect('core:completar_beneficiario', benef_id=beneficiario.id, benef_tarjeta=benef_tarj)
            except Beneficiario.DoesNotExist:
                return render(request, 'extras/generic_form.html', {'titulo': "INGRESE DATOS", 'form': form, 'boton': "ACCEDER", 'message': 'No existe beneficiario con estos datos.'})
        else:            
            return render(request, 'extras/generic_form.html', {'titulo': "INGRESE DATOS", 'form': form, 'boton': "ACCEDER",'message': 'Formulario no valido.'})

#def completar_beneficiario(request, benef_id, benef_tarjeta):
    #message = ''
    #template_name = 'beneficiarios.html'
    #if request.method == 'GET':
        #message = 'COMPLETE SUS DATOS'
        #beneficiario = Beneficiario.objects.get(id=benef_id, tarjeta=benef_tarjeta)
        #benefform = BeneficiarioForm(instance=beneficiario)                        
        #formset = FamiliarFormset(queryset=Familiar.objects.filter(beneficiario=beneficiario))
        #formset2 = ConyugeFormset(queryset=Conyuge.objects.filter(beneficiario=beneficiario))               
    #elif request.method == 'POST':
        #beneficiario = Beneficiario.objects.get(pk=benef_id)
        #benefform = BeneficiarioForm(request.POST, instance=beneficiario)        
        #formset = FamiliarFormset(request.POST)
        #formset2 = ConyugeFormset(request.POST)            
        #if benefform.is_valid() and formset.is_valid() and formset.is_valid():
            #beneficiario = benefform.instance
            #beneficiario.save()            
            #message = "Se ha guardado la informacion indicada."
            #for form in formset:
                #familiar = form.save(commit=False)
                #if familiar.dni:
                    #familiar.beneficiario = beneficiario
                    #try:
                        #Si existia y tenemos que modificarlo
                        #Familiar.objects.get(dni=familiar.dni).delete()
                    #except Familiar.DoesNotExist:
                        #pass
                    #familiar.save()
            #for form2 in formset2:
                #conyuge = form.save(commit=False)
                #if conyuge.dni:
                    #conyuge.beneficiario = beneficiario
                    #try:
                        #Si existia y tenemos que modificarlo
                        #Conyuge.objects.get(dni=conyuge.dni).delete()
                    #except Conyuge.DoesNotExist:
                        #pass
                    #conyuge.save()            
            #return redirect('core:completar_beneficiario', benef_id=benef_id, benef_tarjeta=benef_tarjeta)           
        #else:
            #message = "Se encontro un error en la carga"
    #return render(request, template_name, {
    #    'form': benefform,
    #   'formset': formset,
    #   'formset2': formset2,        
    #    'message': message,
    #})

def completar_beneficiario(request, benef_id, benef_tarjeta):
    message = ''
    if benef_id:
        #beneficiario = Beneficiario.objects.get(id=benef_id, tarjeta=benef_tarjeta)
        beneficiario = Beneficiario.objects.get(pk=benef_id)
    else:
        beneficiario = Beneficiario()

    benefform = BeneficiarioForm(instance=beneficiario) 
    formset = FamiliarFormset(instance=beneficiario)
    formset2 = ConyugeFormset(instance=beneficiario) 

    if request.method == 'POST':        
        benefform = BeneficiarioForm(request.POST)
        if benef_id:
            benefform = BeneficiarioForm(request.POST, instance=beneficiario)
        
        formset = FamiliarFormset(request.POST)
        formset2 = ConyugeFormset(request.POST)

        if benefform.is_valid():
            editar_beneficiario = benefform.save(commit=False)
            editar_beneficiario.save()            
            
            if formset.is_valid():
                formset.instance = editar_beneficiario
                formset.save()

            if formset2.is_valid():
                formset2.instance = editar_beneficiario
                formset2.save() 

                return redirect('core:completar_beneficiario', benef_id=benef_id, benef_tarjeta=benef_tarjeta)
             
        else:
            message = "Se encontro un error en la carga"
    return render(request, 'beneficiarios.html', {'form': benefform,'formset': formset,'formset2': formset2, 'message': message,})

def delete_familiar(request, benef_id, benef_tarjeta, familiar_id):
    familiar = Familiar.objects.get(pk=familiar_id)
    if familiar.beneficiario.pk == benef_id:
        familiar.delete()
    return redirect('core:completar_beneficiario', benef_id=benef_id, benef_tarjeta=benef_tarjeta)

#Consultas
def get_consultas(request):
    return render(request, 'consultas.html', {})
