from django.conf.urls import url
from django.urls import path

#Import personales
from . import views

app_name = 'core'
urlpatterns = [
    #Basicas:
    url(r'^$', views.home, name='home'),
    path('completar/', views.get_tarjeta, name='get_tarjeta'),
    path('consultas/', views.get_consultas, name='get_consultas'),
    path('completar/<int:benef_id>/<int:benef_tarjeta>/', views.completar_beneficiario, name='completar_beneficiario'),
    path('completar/<int:benef_id>/<int:benef_tarjeta>/<int:familiar_id>', views.delete_familiar, name='delete_familiar'),
    
    #File uploads:
    path('upload_escuelas/', views.upload_escuelas, name='upload_escuelas'),
    path('upload_tarjetas/', views.upload_tarjetas, name='upload_tarjetas'),
        
    #File Download
    path('download_beneficiarios/', views.download_beneficiarios, name='download_beneficiarios'),
    path('download_familiares/', views.download_familiares, name='download_familiares'),
    path('download_conyuges/', views.download_conyuges, name='download_conyuges'),
    
]