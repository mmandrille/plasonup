#Imports Python
from datetime import timedelta
import datetime
#Imports del framework Django
from django.utils import timezone
from django import forms
from django.forms import ModelForm, modelformset_factory
from django.forms.models import inlineformset_factory
from .choices import TIPO_TRABAJO, TIPO_BENEFICIO

#Imports de la app:
from .models import Beneficiario, Familiar, Conyuge
#Definimos nuestros formularios
class UploadCsv(forms.Form):
    csvfile = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))

class TarjetaForm(forms.Form):
    tarjeta = forms.IntegerField(min_value=000000000, max_value=999999999)
    dni = forms.IntegerField()

class BeneficiarioForm(forms.ModelForm):    
    class Meta:
        model = Beneficiario
        fields = '__all__'
        widgets = {
            'cuil': forms.TextInput(attrs={'placeholder': 'Introduzca CUIL'}),
            'apellidos': forms.TextInput(attrs={'placeholder': 'Introduzca Apellido'}),
            'nombres': forms.TextInput(attrs={'placeholder': 'Introduzca Nombre'}),
            'tel_celular': forms.TextInput(attrs={'placeholder': 'Introduzca su número de celular'}),
            'tel_fijo': forms.TextInput(attrs={'placeholder': 'Introduzca su número de teléfono fijo'}),
            'email': forms.TextInput(attrs={'placeholder': 'Introduzca su email'}),
            'fecha_nacimiento': forms.DateInput(attrs={'value': datetime.datetime.now().strftime('%d/%m/%Y')}),
            'direccion_calle': forms.TextInput(attrs={'placeholder': 'Introduzca calle donde vive'}),
            'direccion_numero': forms.TextInput(attrs={'placeholder': 'Introduzca Número'}),
            'direccion_barrio': forms.TextInput(attrs={'placeholder': 'Introduzca Barrio'}),
            'direccion_manzana': forms.TextInput(attrs={'placeholder': 'Introduzca Manzana'}),
            'direccion_lote': forms.TextInput(attrs={'placeholder': 'Introduzca Lote'}),
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['tarjeta'].widget.attrs.update({'readonly': True})
            self.fields['dni'].widget.attrs.update({'readonly': True})


class FamiliarForm(forms.ModelForm):
    class Meta:
        model = Familiar
        fields = '__all__'
        exclude = ('beneficiario', 'trabajo', 'beneficio',)

class ConyugeForm(forms.ModelForm):
    class Meta:
        model = Conyuge
        fields = '__all__'
        exclude = ('beneficiario',)

FamiliarFormset = inlineformset_factory(Beneficiario, Familiar,  FamiliarForm, extra=2, can_delete = False)
ConyugeFormset = inlineformset_factory(Beneficiario, Conyuge, ConyugeForm, extra=2, can_delete = False)


