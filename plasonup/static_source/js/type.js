$(document).ready(function(){
    $('#id_cuil').attr('maxlength','13');    
});


$(document).ready(function(){
    $('#id_dni').numeric();
    $('#id_dni').attr('maxlength','8');    
});


$(document).ready(function(){
    $('#id_tarjeta').numeric();
    $('#id_tarjeta').attr('maxlength','9');  
});


//$(document).ready(function () {
    //$('#id_dni')    
    //.keypress(function (event) {
        //if (event.which < 48 || event.which > 57 || this.value.length === 8) {
            //return false;
        //}
    //});
//});

$(document).ready(function(){
    $('#id_tel_celular').numeric();
    $('#id_tel_celular').attr('maxlength','20');   
});

$(document).ready(function(){
    $('#id_tel_fijo').numeric();
    $('#id_tel_fijo').attr('maxlength','15');
});

$(document).ready(function(){
    $('#id_direccion_numero').numeric();
    $('#id_direccion_numero').attr('maxlength','10');   
});


$(document).ready(function () {

    for (var i=0; i<5; i++) {
        $('#id_familiares-'+ i +'-dni').numeric();
        $('#id_familiares-'+ i +'-dni').attr('maxlength','8');      
    }
});

$(document).ready(function () {

    for (var i=0; i<5; i++) {
        $('#id_conyuges-'+ i +'-dni').numeric();
        $('#id_conyuges-'+ i +'-dni').attr('maxlength','8');         
    }
});

$(document).ready(function () {

    for (var i=0; i<5; i++) {
        $('#id_conyuges-'+ i +'-cuil').attr('maxlength','13');        
    }
});